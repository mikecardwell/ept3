const svg = `<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ev="http://www.w3.org/2001/xml-events" width="60" height="60" version="1.1" baseProfile="full"
onload="init(evt)">
  <script type="text/javascript">
    function init( evt ){
      try {
        alert('Wow. JavaScript hidden inside SVG\'s executes in your email client. That\'s a potentially serious security proble
m. Please contact me using the form at https://grepular.com/contact about this.');
      } catch(e){}
    }
  </script>
  <text x="0" y="0" style="background-image:url('%%BASE%%svgBackgroundImg')"/>
</svg>`;

export default svg;
