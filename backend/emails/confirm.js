export const en = {

    from: '"Email Privacy Tester" <noreply@%%MAIL_SENDER_DOMAIN%%>',

    subject: 'EPT - Confirm Your Email',

    list: {
      unsubscribe: {
        url:     '%%FRONTEND_URL%%/optout?code=%%CODE%%',
        comment: 'Opt out from further emails from %%FRONTEND_URL%%',
      },
    },

    text: `Hi there,

This email was triggered by somebody at IP address %%IP%% (hopefully you) entering the email address %%EMAIL%% into the form at %%FRONTEND_URL%%. If this was not you, please don't hit the spam button in your email client as it may make it more difficult for me to send email to others who have legitimately requested it. Instead, visit the optout link at the end of this email and you will never receive any email from this service again.

If it *was* you, please visit the following link so you can initiate tests and view their results:

 Confirm: %%FRONTEND_URL%%/test?code=%%CODE%%

If you find this service useful, and have the means to do so, please consider making a small donation:

 Donate: %%FRONTEND_URL%%/donate

If you run an email service, you may find another free project I created useful:

 ParseMail: https://www.parsemail.org/

Here is my tech blog:

 Tech Blog: https://www.grepular.com/blog/

And finally, here is the optout link:

 Opt out: %%FRONTEND_URL%%/optout?code=%%CODE%%

Regards,

Mike Cardwell <mike.cardwell@grepular.com>

Email Privacy Tester   - https://www.emailprivacytester.com/
Built by Mike Cardwell - https://www.grepular.com/`,

};

export const es = {

    from: '"Email Privacy Tester" <noreply@%%MAIL_SENDER_DOMAIN%%>',

    subject: 'EPT - Email de confirmación',

    list: {
      unsubscribe: {
        url:     '%%FRONTEND_URL%%/es/optout?code=%%CODE%%',
        comment: 'No participación %%FRONTEND_URL%%',
      },
    },

    text: `Hola,

Este email se activó porque alguien con la dirección IP %%IP%% (esperamos que haya sido usted) escribió la dirección de email %%EMAIL%% en el formulario de %%FRONTEND_URL%%. Si no fue usted, por favor no le dé al botón de spam de su cliente de correo, pues podría dificultarme el envío de email a otros que lo han pedido explícitamente. En su lugar, visite el enlace de salida al final de este email y nunca volverá a recibir un email de este servicio.

Si *fue* usted, por favor, visite el siguiente enlace para comenzar los test y ver los resultados:

 Confirmar: %%FRONTEND_URL%%/es/test?code=%%CODE%%

Por favor, si considera útil este servicio y tiene los medios para ello, considere hacer una pequeña donación:

 Donar: %%FRONTEND_URL%%/es/donate

Si tiene un servicio de email, puede que le interese otro proyecto gratuito que tengo:

 ParseMail: https://www.parsemail.org/

Aquí está mi blog de tecnología:

 Tech Blog: https://www.grepular.com/blog/
 
Y finalmente, aquí está el enlace de no participación:

 No participación: %%FRONTEND_URL%%/es/optout?code=%%CODE%%
 
Saludos,

Mike Cardwell <mike.cardwell@grepular.com>

Email Privacy Tester   - https://www.emailprivacytester.com/
Built by Mike Cardwell - https://www.grepular.com/`

};