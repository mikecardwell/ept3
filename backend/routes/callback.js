import Callbacks from '../lib/db/model/Callbacks';
import Tests     from '../lib/db/model/Tests';
import * as AllTests from '../lib/tests';

import { newCallback } from '../lib/longPollManager';

export default async function callbackRoute (req, res) {

  const { code = '', test: testName } = req.query;

  try {

    if (!(testName in AllTests)) {
      throw new Error('Unknown test');
    }

    const test = await Tests.findOne({ _id: code });
    if (!test) throw new Error('Not Found');

    if (!test.accessed) {
      test.accessed = true;
      await test.save();
    }

    const { type } = AllTests[testName];
    const ip = type === 'dns'
      ? req.query.ip
      : req.remote().client;

    const callback = new Callbacks({
      code,
      ip,
      test: testName,
      http: type === 'http' ? {
        httpUserAgent:    req.headers['user-agent'],
        httpForwardedFor: ip.httpForwardedFor,
      } : undefined,
    });
    await callback.save();

    newCallback(code);

    return res.json({ ok: `Successfully recorded ${testName} test` });

  } catch (err) {

    console.log(`Error recording "${testName}" test:`, err.message); // eslint-disable-line no-console
    res.json({ error: `Failed to record ${testName} test` });

  }

}
