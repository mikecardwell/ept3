import * as tests from '../lib/tests';

export default function testListRoute (req, res) {
  const { lang='en' } = req.query;

  res.json({
    ok: Object.keys(tests).reduce((o, id) => {
      const data = tests[id](lang);
      return {
        ...o,
        [id]: {
          ...data,
          desc: data.desc.trim(),
        },
      };
    }, {})
  });
}
