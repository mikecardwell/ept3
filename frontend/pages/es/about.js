import React, { Component } from 'react';

import AboutPage from '../../lib/components/AboutPage';

export default class About extends Component {

  render() {
    return (
      <AboutPage lang="es" { ...this.props }/>
    );
  }

}
