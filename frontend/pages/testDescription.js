import React, { Component } from 'react';

import TestDescriptionPage from '../lib/components/TestDescriptionPage';

import api from '../lib/api';

let testList = null;

export default class TestDescription extends Component {

  static async getInitialProps({ query }) {

    if (!testList) {
      try {

        testList = await api.get('testList', { lang: 'en' });

      } catch (err) {

        // @todo: 404?

      }
    }

    return {
      ...testList[query.test],
    };
  }

  render() {
    return (
      <TestDescriptionPage lang="en" { ...this.props }/>
    );
  }
}
