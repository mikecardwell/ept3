import React, { Component } from 'react';

import HomePage from '../lib/components/HomePage';

export default class Home extends Component {

  static getInitialProps ({ query }) {
    const { email = '' } = query;
    return {
      initialEmail: email,
    };
  }

  render() {
    return (
      <HomePage lang="en" { ...this.props }/>
    );
  }

}
