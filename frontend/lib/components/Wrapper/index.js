import React, { Component } from 'react';

import ServerDate from '../../ServerDate';
import Head from 'next/head';
import Link from 'next/link';
import css  from 'next/css';

import Footer from './Footer';

const style = {
  root: css({
    flexGrow:      1,
    minHeight:     '100%',
    display:       'flex',
    flexDirection: 'column',
  }),
  header: {
    root: css({
      display:         'flex',
      flexWrap:        'wrap',
      justifyContent:  'center',
      backgroundColor: '#333',
      color:           'white',
      minHeight:       '10vh',
      alignItems:      'center',
      padding:         '1em',
    }),
    title: {
      root: css({
        textAlign:  'center',
        flexGrow:   1,
        lineHeight: 1.5,
        margin:     0,
        padding:    '0 0.5em',
      }),
      link: css({
        display:        'inline-block',
        textDecoration: 'none',
        textAlign:      'center',
        color:          'white',
        '::first-letter': {
          color: 'orange',
        },
      }),
    },
    nav: {
      root: css({
        display:        'flex',
        alignItems:     'center',
        flexWrap:       'wrap',
        justifyContent: 'space-between',
        padding:        '0.5em 1em 0.5em 0',
      }),
      link: css({
        color:      'white',
        textShadow: '1px 1px 1px black',
        marginLeft: '2em',
        fontWeight: 'bold',
        padding:    '0.5em 0',
      }),
      flag: css({
        marginLeft:     '2em',
        fontWeight:     'bold',
        padding:        '0.5em 0',
        textDecoration: 'none',
      }),
    },
  },
  main: {
    root: css({
      flexGrow:       1,
      display:        'flex',
      flexDirection:  'column',
      justifyContent: 'center',
      alignItems:     'center',
      padding:        '0 1em',
    }),
    title: css({
      backgroundColor: '#f0eeee',
      padding:         '0.5em 3em',
      lineHeight:      '2',
      textAlign:       'center',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    }),
  },
};

let updateDiffCalled = false;

export default class Wrapper extends Component {

  componentWillMount() {
    if (typeof window !== 'undefined') {
      this.when = document.querySelector('#__next > div').dataset.when;
      if (!updateDiffCalled) {
        updateDiffCalled = true;
        ServerDate.updateDiff(this.when - Date.now());
      }
    } else {
      this.when = Date.now();
    }
  }

  render() {
    const { lang, title } = this.props;
    const root = (!lang || lang === 'en') ? '/' : `/${lang}/`;

    return (
      <div className={ style.root } data-when={ this.when }>
        <Head>
          <link rel="stylesheet" type="text/css" href="/static/common.css" />
          <link rel="stylesheet" type="text/css" href="/static/fonts/index.css" />
        </Head>
        <header className={ style.header.root }>
          <h1 className={ style.header.title.root }>
            <Link href={ root }>
              <a href={ root } className={ style.header.title.link } title={ lang === 'es' ? 'Inicio' : 'Home' }>Email Privacy Tester</a>
            </Link>
          </h1>
          <nav className={ style.header.nav.root }>
            {
              this.flagSwitcher()
            }
            <Link href={ root }>
              <a className={ style.header.nav.link } href={ root }>{ lang === 'es' ? 'Inicio' : 'Home' }</a>
            </Link>
            <Link href={ `${root}about` }>
              <a className={ style.header.nav.link } href={ `${root}about` }>{ lang === 'es' ? 'Sobre Nosotros' : 'About' }</a>
            </Link>
            <Link href={ `${root}privacy` }>
              <a className={ style.header.nav.link } href={ `${root}privacy` }>{ lang === 'es' ? 'Política de Privacidad' : 'Privacy' }</a>
            </Link>
            <Link href={ `${root}donate` }>
              <a className={ style.header.nav.link } href={ `${root}donate` }>{ lang === 'es' ? 'Donaciones' : 'Donate' }</a>
            </Link>
            <a className={ style.header.nav.link } href="https://grepular.com">{ lang === 'es' ? 'Contacto' : 'Contact' }</a>
          </nav>
        </header>
        <main className={ style.main.root }>
          {
            title && (
              <h2 className={ style.main.title }>{ title }</h2>
            )
          }
          { this.props.children }
        </main>
        <Footer lang={ lang }/>
      </div>
    );

  }

  flagSwitcher() {
    const { lang, url: { pathname } } = this.props;

    const icon = lang === 'es' ? '🇬🇧' : '🇪🇸';

    const uri = lang === 'es'
      ? pathname.replace(/^(\/*es)+/, '')
      : `/es${pathname}`;

    const title = lang === 'es' ? 'Inglés' : 'Switch to Spanish';

    return (
      <a href={ uri }
        className = { style.header.nav.flag }
        title     = { title }
        onClick   = { e => this.switchLanguage(e, uri, lang === 'es' ? 'en' : 'es') }
      >
        { icon }
      </a>
    );
  }

  switchLanguage(e, uri, lang) {
    e.preventDefault();
    try {
      localStorage.setItem('lang', lang);
    } catch(err) {
      console.error(err);
    }
    this.props.url.pushTo(uri);
  }
}
