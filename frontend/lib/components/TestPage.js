import React, { Component } from 'react';

import Wrapper from './Wrapper';

import TestResults       from './TestResults';
import TestResultsIPs    from './TestResults/IPs';
import TestResultsAgents from './TestResults/Agents';

import api from '../api';

import css  from 'next/css';
import Link from 'next/link';

const style = {
  error: css({ maxWidth: '30em', color: 'red' }),
  actions: {
    root:   css({ display: 'flex' }),
    action: css({ margin: '0 0.5em' }),
  },
  resultsSummary: css({
    display:        'flex',
    justifyContent: 'space-between',
    maxWidth:       '60em',
  }),
};

function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export default class TestPage extends Component {

  constructor (props) {

    super(props);
    this.state = {
      submitting:   false,
      error:        null,
      results:      props.initialResults || [],
      testSent:     props.testSent,
      testSentWait: false,
    };
    this.send = this.send.bind(this);
    this.checkResults  = this.checkResults.bind(this);
    this.deleteResults = this.deleteResults.bind(this);
  }

  componentDidMount() {

    this.mounted = true;
    this.checkResults();

  }

  componentWillUnmount() {
    this.mounted = false;
  }

  render() {

    const { email, tests, lang } = this.props;
    const { error, submitting, results, testSent, testSentWait } = this.state;

    if (!email) {
      return lang === 'es' ? (
        <Wrapper { ...this.props } title="Contador caducado">
          <p>
            Parece que el enlace que ha visitado ha caducado. Por favor
            intenta <Link href="/">
              <a href="/">confirmar tu dirección de correo electrónico</a>.
            </Link>
          </p>
        </Wrapper>
      ) : (
        <Wrapper { ...this.props } title="Expired Link">
          <p>
            The link you have visited looks like it has expired. Please
            try <Link href="/">
              <a href="/">reconfirming your email address</a>.
            </Link>
          </p>
        </Wrapper>
      );
    }

    const title = (lang === 'es' ? 'Probando' : 'Testing') + ` ${email}`;

    return (
      <Wrapper { ...this.props } lang={ lang } title={ title }>
        <div className={ style.actions.root }>
          {
            <button
              className = { style.actions.action }
              onClick   = { this.send }
              disabled  = { submitting || testSentWait }
            >
              {
                  submitting   ? (lang === 'es' ? 'Enviando...'            : 'Sending...')
                : testSentWait ? (lang === 'es' ? 'Email enviado'          : 'Email Sent')
                : testSent     ? (lang === 'es' ? 'Enviar Otro Email'      : 'Send Another Email')
                :                (lang === 'es' ? 'Enviar email de prueba' : 'Send Test Email')
              }
            </button>
          }
          {
            results.length ? (
              <button
                className = { style.actions.action }
                onClick   = { this.deleteResults }
                disabled  = { submitting }
              >
                { lang === 'es' ? 'Borrar resultados del test' : 'Delete Test Results' }
              </button>
            ) : null
          }
        </div>
        {
          error && (
            <p className={ style.error }>
              { error }
            </p>
          )
        }
        <div className={ style.resultsSummary }>
          <TestResultsIPs    lang={ lang } results={ results }/>
          <TestResultsAgents lang={ lang } results={ results }/>
        </div>
        <TestResults lang={ lang } tests={ tests } results={ results }/>
      </Wrapper>
    );

  }

  async checkResults() {
    const { code    } = this.props;
    const { results } = this.state;

    if (!this.mounted) return;

    try {

      const newResults = await api.get('testResults', {
        code,
        longPoll: 1,
        since:    results.length ? results[results.length - 1].time : 0,
      });

      if (newResults.length) {

        if (!this.mounted) return;
        return this.setState({
          results: [ ...results, ...newResults ],
        }, this.checkResults);

      }

    } catch (err) {

      await sleep(2000);

    }
    this.checkResults();

  }

  deleteResults() {
    const { code       } = this.props;
    const { submitting } = this.state;
    if (submitting) return;
    this.setState({ error: null, submitting: true }, async () => {
      let newState = { submitting: false };
      try {
        await api.post('deleteResults', { code });
        newState.results = [];
      } catch (err) {
        newState.error = err.message;
      }
      if (this.mounted) this.setState(newState);
    });
  }

  send() {

    const { code, lang } = this.props;

    if (this.state.submitting) return;

    this.setState({
      submitting: true,
      error:      null,
    }, async () => {

      try {

        let t = Date.now();
        await api.post('sendTest', { code, lang });

        t = Date.now() - t;
        if (t < 1500) await sleep(1500 - t);

        this.setState({
          submitting:   false,
          testSentWait: true,
        }, async () => {
          await sleep(2000);
          this.setState({
            testSentWait: false,
            testSent:     true,
          });
        });

      } catch(err) {
        this.setState({
          submitting: false,
          error:      err.message,
        });
      }

    });

  }

}
