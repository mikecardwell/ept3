import React, { Component } from 'react';

import Wrapper from './Wrapper';

import Link from 'next/link';

export default class AboutPage extends Component {

  render() {
    const { lang } = this.props;

    const title = lang === 'es'
        ? 'Sobre el Probador de Privacidad de Email'
        : 'About the Email Privacy Tester';

    return (
      <Wrapper { ...this.props } title={ title }>
        <div>
          {
            lang === 'es' ? (
              <p>
                Algunos clientes de email, cuando leen un email, hacen operaciones
                que entregan información sobre el lector o el remitente del mensaje.
                Si pone su dirección de email en el formulario de
                mi <Link href="/es/">página de inicio</Link>, esta aplicación web le
                enviará un email de confirmación para asegurarse de que es su
                dirección de correo. En ese email habrá otro enlace. Cliquee en ese
                enlace y será llevado a una página donde puede activar emails de
                prueba que le serán enviados. Esos emails de prueba están hechos
                para usar distintas técnicas, que intentan enviar información de
                vuelta al servidor cuando son leídos. Después le mostrará
                los resultados.
              </p>
            ) : (
              <p>
                Some email clients perform operations when reading an email which
                give away information about the reader, to the sender of the message.
                If you enter your email address into the form on
                my <Link href="/">home page</Link>, this web app will send you a
                confirmation email to make sure you own the email address. In that
                email there will be another link. Click that link and you will be
                taken to a page where you can trigger test emails to be sent to you.
                Those test emails are specially crafted to use a variety of
                techniques, to attempt to send information back to this server when
                read. It will then display the results for you.
              </p>
            )
          }
          {
            lang === 'es' ? (
              <p>
                Los resultados son dinámicos, así que al principio dirán que ningún
                test ha tenido éxito. Abra el mensaje en su cliente de email y
                varios test se activarán. Cliquee entonces en “cargar imágenes
                remotas” (o su equivalente) en su cliente de email, y más test
                se activarán.
              </p>
            ) : (
              <p>
                The results are dynamic, so initially they will say that none of the
                tests have succeeded. Open the message in your email client and a
                bunch of tests might be triggered. Then click the "Load remote
                images" button (or equivalent) in your client and more tests may be
                triggered.
              </p>
            )
          }
          {
            lang === 'es' ? (
              <p>
                Si solamente la lectura del mensaje sin seleccionar “cargar
                imágenes remotas” activa alguno de los test, entonces su cliente
                de email tiene un “error de privacidad”, o no ha sido configurado
                óptimamente para ser privado.
              </p>
            ) : (
              <p>
                If merely reading the message without selecting to load remote images
                triggers any of the tests, then either your email client has a
                "privacy bug," or it is not configured for optimal privacy.
              </p>
            )
          }
          {
            lang === 'es' ? (
              <p>
                Me encargo de diferentes sistemas de correo electrónico como forma
                de vida, así que odio el spam más que la mayoría de la gente. Me
                tomo su privacidad muy en serio. Por favor, mire mi página de
                privacidad si quiere saber más.
              </p>
            ) : (
              <p>
                I run email systems for a living so hate spam more than most people.
                I take your privacy extremely seriously. Please check out my privacy
                page if you want to know more.
              </p>
            )
          }
          {
            lang === 'es' ? (
              <p>
                El código fuente completo de esta aplicación está disponible
                en: <a href="https://gitlab.com/grepular/ept3">
                  https://gitlab.com/grepular/ept3
                </a>
              </p>
            ) : (
              <p>
                The full source code for this application is available
                at <a href="https://gitlab.com/grepular/ept3">
                  https://gitlab.com/grepular/ept3
                </a>
              </p>
            )
          }
          {
            lang === 'es' ? '' : (
              <p>
                Thank you to Adrián Losada for contributing a <Link href="es/">Spanish translation</Link> of this website.
              </p>
            )
          }
        </div>
      </Wrapper>
    );
  }

}
