/**
 * Simple library for managing server/client clock diffs
 */

let diff = 0;

export default class ServerDate {

  static updateDiff(d) {
    diff = d;
  }

  constructor() {
    this._now = Date.now();
  }

  now() {
    return this._now - diff;
  }

  date() {
    return new Date(this.now());
  }

}
