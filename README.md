# Email Privacy Tester v3

This is the third version of the Email Privacy Tester. A self-hostable tool for testing email clients for various privacy and security failures. If you don't wish to host your own version, you can access the developers version of it at https://www.emailprivacytester.com